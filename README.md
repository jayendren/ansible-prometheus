Ansible Prometheus
=========

* [The Stack](#the-stack)
* [Requirements](#requirements)
* [Role Variables](#role-variables)
* [Dependencies](#dependencies)
* [Demo](#demo)

### The Stack

- collectd:           http://collectd.org
- prometheus:         http://prometheus.io
- grafana:            http://grafana.org
- collectd_exporter:  https://github.com/prometheus/collectd_exporter
- cadvisor:           https://prometheus.io/docs/instrumenting/exporters
- jmx_exporter:       https://github.com/prometheus/jmx_exporter
- mysql_exporter:     https://github.com/prometheus/mysqld_exporter (TODO)
- postgres_exporter:  https://github.com/wrouesnel/postgres_exporter (TODO)
- blackbox_exporter:  https://github.com/prometheus/blackbox_exporter (TODO)
- alertmanager:       https://prometheus.io/docs/alerting/alertmanager

![](demo/collectd_prom_grafana.png)
![](demo/docker_prom_grafana.png)
![](demo/jmx_tomcat_prom_grafana.png)

Requirements
------------

- metrics from jmx_exporter will need to be setup on prometheus_tomcat_jmx_nodes 
- (jmx exporter tested on tomcat7.x with java 1.7.x)

```
JAVA_OPTS="-Djava.awt.headless=true -Xmx128m -XX:+UseConcMarkSweepGC -Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=9999 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false -javaagent:/usr/local/jmx_exporter/jar/jmx_prometheus_javaagent-0.5.jar=7070:/usr/local/jmx_exporter/conf/tomcat.yml"
```

- FreeBSD 10.x for prometheus_cluster_nodes:     https://www.freebsd.org/where.html

Role Variables
--------------

```

  vars:
      collectd_prometheus: 
        - { enable: "true",
            server: "prometheus.systemerror.co.za",
            port:   "25826",
          }
    - role: collectd
      collectd_plugins_multi:
        network: >-
          <Server 0.0.0.0>
            #SecurityLevel "Encrypt"
            #AuthFile "/etc/collectd/auth_file"
          </Server>
        df: >-
          IgnoreSelected true
      tags: collectd

    - role: prometheus
      prometheus_datadir: "/var/db/prometheus"
      global_scrape_interval: "15s"
      global_evaluation_interval: "15s"
      global_external_labels: "monitor"
      prom_cadvisor_exporter_nodes: 
        - coreos01.systemerror.co.za
        - coreos02.systemerror.co.za
        - docker.systemerror.co.za
      prom_cadvisor_port: 8080
      prom_jmx_tomcat_exporter_nodes:
        - app01.systemerror.co.za
      prom_jmx_tomcat_exporter_port: 7070        
      prom_exporters_multi:
        prometheus: |
          scrape_interval: 5s
              target_groups:
              - targets: ['localhost:9090']
        collectd_exporter: |
          scrape_interval: "15s"
              target_groups:
              - targets: [ 'localhost:9103' ]

    - role: alertmanager
      alertmanager_smtp_smarthost: "localhost:25"
      alertmanager_smtp_from:      "root@systemerror.co.za"
      alertmanager_rules:          ## TODO
        - collectd_alerts.yml      ## TODO
        - container_alerts.yml     ## TODO
        - jmx_alerts.yml           ## TODO
        - pgsql_alerts.yml         ## TODO
        - mysql_alerts.yml         ## TODO
      alertmanager_rules_multi:
        alert_instance_down_5_mins: |
          ALERT InstanceDown
            IF up == 0
            FOR 5m
            LABELS { severity = "page" }
            {% raw %}
            ANNOTATIONS {
              summary = "Instance {{ $labels.instance }} down",
              description = "{{ $labels.instance }} of job {{ $labels.job }} has been down for more than 5 minutes.",
            }
            {% endraw %}  

    - role: collectd_exporter
      collectd_exporter_listen: 25826

    - role: grafana
      grafana_setup_user:  "admin"
      grafana_setup_pass:  "admin"
      grafana_prom_dc:     "Prometheus"
      grafana_app_mode:    "production"
      grafana_datadir:     "/var/db/grafana2/"
      grafana_logdir:      "/var/log/grafana2/"
      grafana_pluginsdir:  "/var/db/grafana3/plugins"
      grafana_plugins:
        - grafana-piechart-panel
        - mtanda-histogram-panel
      grafana_protocol:    "http"
      grafana_http_port:   "3000"
      grafana_enable_gzip: "true"      
      grafana_protocol:    "http"
      grafana_http_port:   "3000"
      grafana_enable_gzip: "true"
```      

Dependencies
------------

- [roles/alertmanager](roles/alertmanager/README.md)
- [roles/collectd](roles/collectd/README.md)
- [roles/collectd_exporter](roles/collectd_exporter/README.md)
- [roles/golang](roles/golang/README.md)
- [roles/grafana](roles/grafana/README.md)
- [roles/jmx_exporter](roles/jmx_exporter/README.md)
- [roles/prometheus](roles/prometheus/README.md)

Example Playbook
----------------

Including an example of how to use the role (for instance, with variables passed in as parameters) is always nice for users too:

- site.yml

```
---
- include: collectd.yml
- include: golang.yml
- include: prometheus.yml
- include: alertmanager.yml
- include: collectd_exporter.yml
- include: jmx_exporter.yml
- include: grafana.yml

```

-  collectd.yml

```
- hosts: collectd
  tasks: []
  vars:
      collectd_prometheus: 
        - { enable: "true",
            server: "prometheus.systemerror.co.za",
            port:   "25826",
          }
  roles:
    - role: collectd
      collectd_plugins_multi:
        network: >-
          <Server 0.0.0.0>
            #SecurityLevel "Encrypt"
            #AuthFile "/etc/collectd/auth_file"
          </Server>
        df: >-
          IgnoreSelected true
      tags: collectd
```
- golang.yml

```
- hosts: prometheus_cluster_nodes
  tasks: []
  roles:
    - role: golang
```

-  prometheus.yml

```

    - hosts: prometheus_cluster_nodes
      tasks: []
      roles:
        - role: prometheus
          prometheus_datadir: "/var/db/prometheus"
          global_scrape_interval: "15s"
          global_evaluation_interval: "15s"
          global_external_labels: "monitor"
          prom_cadvisor_exporter_nodes: 
            - coreos01.systemerror.co.za
            - coreos02.systemerror.co.za
            - docker.systemerror.co.za
          prom_cadvisor_port: 8080
          prom_jmx_tomcat_exporter_nodes:
            - app01.systemerror.co.za
          prom_jmx_tomcat_exporter_port: 7070        
          prom_exporters_multi:
            prometheus: |
              scrape_interval: 5s
                  target_groups:
                  - targets: ['localhost:9090']
            collectd_exporter: |
              scrape_interval: "15s"
                  target_groups:
                  - targets: [ 'localhost:9103' ]
```

- alertmanager.yml

```
- hosts: prometheus_cluster_nodes
  tasks: []
  roles:
    - role: alertmanager
      alertmanager_smtp_smarthost: "localhost:25"
      alertmanager_smtp_from:      "root@systemerror.co.za"
      alertmanager_rules:   ## TODO
        - collectd_alerts.yml
        - container_alerts.yml
        - jmx_alerts.yml
        - pgsql_alerts.yml
        - mysql_alerts.yml
      alertmanager_rules_multi:
        alert_instance_down_5_mins: |
          ALERT InstanceDown
            IF up == 0
            FOR 5m
            LABELS { severity = "page" }
            {% raw %}
            ANNOTATIONS {
              summary = "Instance {{ $labels.instance }} down",
              description = "{{ $labels.instance }} of job {{ $labels.job }} has been down for more than 5 minutes.",
            }
            {% endraw %}  
```

- collectd_exporter.yml

```
- hosts: prometheus_cluster_nodes
  tasks: []
  roles:
    - role: collectd_exporter
      collectd_exporter_listen: 25826
```

- jmx_exporter.yml

```
    - hosts: prometheus_tomcat_jmx_nodes
      tasks: []
      roles:
        - role: jmx_exporter
```

- grafana.yml

```
- hosts: prometheus_cluster_nodes
  tasks: []
  roles:
    - role: grafana
      grafana_setup_user:  "admin"
      grafana_setup_pass:  "admin"
      grafana_prom_dc:     "Prometheus"
      grafana_app_mode:    "production"
      grafana_datadir:     "/var/db/grafana3/"
      grafana_logdir:      "/var/log/grafana3/"
      grafana_pluginsdir:  "/var/db/grafana3/plugins"
      grafana_plugins:
        - grafana-piechart-panel
        - mtanda-histogram-panel
      grafana_protocol:    "http"
      grafana_http_port:   "3000"
      grafana_enable_gzip: "true"
```

Demo
------------------

[![asciicast](https://asciinema.org/a/2rf4wfmsgej7wesgpq5phn2pm.png)](https://asciinema.org/a/2rf4wfmsgej7wesgpq5phn2pm)

License
-------

BSD

Author Information
------------------

Jayendren Maduray <jayendren@gmail.com>
