Ansible Prometheus Alertmanager
=========

### About

- prometheus alert manager:         https://github.com/prometheus/alertmanager

Requirements
------------

- FreeBSD https://www.freebsd.org/
- https://gitlab.com/jayendren/ansible-prometheus

Role Variables
--------------

```

alertmanager_xmode:                    0555
alertmanager_rmode:                    0640
alertmanager_dmode:                    0755
alertmanager_group:                    "alertmanager"
alertmanager_owner:                    "alertmanager"
alertmanager_filebackup:               "false"
alertmanager_data_dir:                 "/var/db/alertmanager"
alertmanager_smtp_smarthost:           "localhost:25"
alertmanager_smtp_from:                "root@systemerror.co.za"


alertmanager_vars_FreeBSD:
  - { alertmanager_conf_dir:           "/usr/local/etc/alertmanager",
      alertmanager_rule_path:          "/usr/local/etc/alertmanager/rules.d",
      alertmanager_rule_ext:           "*.rules",
      alertmanager_rule_template_src:  "alertmanager_rules_template.j2",
      alertmanager_rule_template_dst:  "/usr/local/etc/alertmanager/rules.d/default.rules",
      alertmanager_bin_dst:            "/usr/local/sbin/alertmanager",
      alertmanager_bin_src:            "alertmanager_0_1_1_freebsd_amd64.bin",
      alertmanager_conf_dst:           "/usr/local/etc/alertmanager/alertmanager.yml",
      alertmanager_conf_src:           "alertmanager.yml.j2",
      alertmanager_rc_conf_dst:        "/usr/local/etc/rc.d/alertmanager",
      alertmanager_rc_conf_src:        "alertmanager_freebsd_rc.j2",
      alertmanager_rc_confd_dst:       "/etc/rc.conf.d/alertmanager",
      alertmanager_rc_confd_src:       "alertmanager_freebsd_rconfd.j2",
      alertmanager_svc:                "alertmanager",
      alertmanager_shell:              "/usr/bin/nologin",
    }
            
```      

Dependencies
------------

- None

Example Playbook
----------------

Including an example of how to use the role (for instance, with variables passed in as parameters) is always nice for users too:

```
- hosts: prometheus_cluster_nodes
  tasks: []
  roles:
    - role: alertmanager
      alertmanager_smtp_smarthost: "localhost:25"
      alertmanager_smtp_from:      "root@systemerror.co.za"
      alertmanager_rules:
        ## TODO- collectd_alerts.yml
        ## TODO- container_alerts.yml
        - jmx_alerts.yml
        ## TODO- pgsql_alerts.yml
        ## TODO- mysql_alerts.yml
      alertmanager_rules_multi:
        alert_instance_down_5_mins: |
          ALERT InstanceDown
            IF up == 0
            FOR 5m
            LABELS { severity = "page" }
            {% raw %}
            ANNOTATIONS {
              summary = "Instance {{ $labels.instance }} down",
              description = "{{ $labels.instance }} of job {{ $labels.job }} has been down for more than 5 minutes.",
            }
            {% endraw %}   
```

License
-------

BSD

Author Information
------------------

Jayendren Maduray <jayendren@gmail.com>
