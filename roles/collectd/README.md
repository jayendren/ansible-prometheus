CollectD
=========

- collectd: The system statistics collection daemon http://collectd.org/

Requirements
------------

- ArchLinux
- Debian
- FreeBSD
- RHEL & Clones

Role Variables
--------------

```

      collectd_prometheus: 
        - { enable: "true",
            server: "prometheus.systemerror.co.za",
            port:   "25826",
          }

      collectd_plugins_multi:
        network: >-
          <Server 0.0.0.0>
            #SecurityLevel "Encrypt"
            #AuthFile "/etc/collectd/auth_file"
          </Server>
        df: >-
          IgnoreSelected true
      tags: collectd
```

Dependencies
------------

- None

Example Playbook
----------------

Including an example of how to use this role (for instance, with variables passed in as parameters) is always nice for users too:

```
- hosts: collectd
  tasks: []
  vars:
      collectd_prometheus: 
        - { enable: "true",
            server: "prometheus.systemerror.co.za",
            port:   "25826",
          }
  roles:
    - role: collectd
      collectd_plugins_multi:
        network: >-
          <Server 0.0.0.0>
            #SecurityLevel "Encrypt"
            #AuthFile "/etc/collectd/auth_file"
          </Server>
        df: >-
          IgnoreSelected true
      tags: collectd
```

License
-------

BSD

Author Information
------------------

Jay Maduray <jayendren@gmail.com>
