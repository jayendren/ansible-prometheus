Ansible Prometheus CollectD Exporter
=========

### About

- collectd_exporter:  https://github.com/prometheus/collectd_exporter


Requirements
------------

- FreeBSD https://www.freebsd.org/
- https://gitlab.com/jayendren/ansible-prometheus

Role Variables
--------------

```
    - role: collectd_exporter
      collectd_exporter_listen: 25826
```      

Dependencies
------------

- a node running collectd configured to post metrics to prometheus

```
## Prometheus collectd_exporter
LoadPlugin network
<Plugin network>
  Server "prometheus.systemerror.co.za" "25826"
</Plugin>
```

Example Playbook
----------------

Including an example of how to use the role (for instance, with variables passed in as parameters) is always nice for users too:


```
    - hosts: prometheus_cluster_nodes
      tasks: []
      roles:
        - role: golang
        - role: collectd_exporter
          collectd_exporter_listen: 25826
```


License
-------

BSD

Author Information
------------------

Jayendren Maduray <jayendren@gmail.com>
