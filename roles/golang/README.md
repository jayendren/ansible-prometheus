Ansible golang
=========

### About

- https://golang.org/


Requirements
------------

- FreeBSD https://www.freebsd.org/
- https://golang.org/dl/

Role Variables
--------------

```
golang_vars_FreeBSD:
  - { golang_pkg: "go",
    }
```    

Dependencies
------------

- None


Example Playbook
----------------

Including an example of how to use the role (for instance, with variables passed in as parameters) is always nice for users too:


```
- hosts: prometheus_cluster_nodes
  tasks: []
  roles:
    - role: golang
```


License
-------

BSD

Author Information
------------------

Jayendren Maduray <jayendren@gmail.com>
