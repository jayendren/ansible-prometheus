Ansible grafana
=========

### About

- grafana.org


Requirements
------------

- FreeBSD https://www.freebsd.org/
- https://gitlab.com/jayendren/ansible-prometheus

Role Variables
--------------

```
---

grafana_xmode:                 0555
grafana_rmode:                 0640
grafana_dmode:                 0755
grafana_group:                 "grafana"
grafana_owner:                 "grafana"
grafana_setup_user:            "admin"
grafana_setup_pass:            "admin"
grafana_prom_dc:               "Prometheus"
grafana_app_mode:              "production"
grafana_datadir:               "/var/db/grafana3/"
grafana_logdir:                "/var/log/grafana3/"
grafana_pluginsdir:            "/var/db/grafana3/plugins"
grafana_plugins_install:       "grafana-cli --pluginsDir {{ grafana_pluginsdir }} plugins install"
grafana_plugins:
  - grafana-piechart-panel
  - mtanda-histogram-panel
grafana_protocol:              "http"
grafana_http_port:             "3000"
grafana_enable_gzip:           "true"
grafana_filebackup:            "false"

grafana_prom_dc_init:          "/tmp/prom_dc_init.sh"
grafana_prom_collectd_src:     "collectd.json.j2"
grafana_prom_collectd_dst:     "/tmp/collectd.json"
grafana_prom_docker_src:       "docker.json.j2"
grafana_prom_docker_dst:       "/tmp/docker.json"
grafana_prom_jmx_src:          "jmx.json.j2"
grafana_prom_jmx_dst:          "/tmp/jmx.json"
grafana_prom_src:              "prometheus.json.j2"
grafana_prom_dst:              "/tmp/prometheus.json"

grafana_vars_FreeBSD:
  - { grafana_pkg:             "grafana3",
      grafana_rc_dst:          "/usr/local/etc/rc.d/grafana3",
      grafana_rc_src:          "grafana_freebsd_rc.j2",
      grafana_rcconfd_src:     "grafana_freebsd_rconfd.j2",
      grafana_rcconfd_dst:     "/etc/rc.conf.d/grafana3",
      grafana_confdir:         "/usr/local/etc/grafana3",
      grafana_conf_src:        "grafana.conf.j2",
      grafana_conf_dst:        "/usr/local/etc/grafana3/grafana3.conf",
      grafana_svc:             "grafana3",
      grafana_shell:           "/usr/bin/nologin",
    }
    
```    

Dependencies
------------

- None


Example Playbook
----------------

Including an example of how to use the role (for instance, with variables passed in as parameters) is always nice for users too:


```
- hosts: prometheus_cluster_nodes
  tasks: []
  roles:
              
    - role: grafana
      grafana_setup_user:  "admin"
      grafana_setup_pass:  "admin"
      grafana_prom_dc:     "Prometheus"
      grafana_app_mode:    "production"
      grafana_datadir:     "/var/db/grafana2/"
      grafana_logdir:      "/var/log/grafana2/"
      grafana_protocol:    "http"
      grafana_http_port:   "3000"
      grafana_enable_gzip: "true"
      grafana_plugins:
        - grafana-piechart-panel
        - mtanda-histogram-panel
```


License
-------

BSD

Author Information
------------------

Jayendren Maduray <jayendren@gmail.com>
