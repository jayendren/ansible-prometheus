#!/usr/bin/env bash

for i in collectd docker prometheus jmx; do
  curl -i -H "Content-Type: application/json" -X DELETE "http://admin:admin@localhost:3000/api/dashboards/db/$i";
  echo
done