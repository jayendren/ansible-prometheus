Ansible jmx_exporter
=========

### About

- Install jmx-exporter java tomcat app servers: https://github.com/prometheus/jmx_exporter


Requirements
------------

- FreeBSD https://www.freebsd.org/
- https://gitlab.com/jayendren/ansible-prometheus
- Tomcat startup JAVA_OPTS

```
JAVA_OPTS="-Djava.awt.headless=true -Xmx128m -XX:+UseConcMarkSweepGC -Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=9999 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false -javaagent:/usr/local/jmx_exporter
```

Role Variables
--------------

- None

Dependencies
------------

- None


Example Playbook
----------------

Including an example of how to use the role (for instance, with variables passed in as parameters) is always nice for users too:


```
- hosts: prometheus_tomcat_jmx_nodes
  tasks: []
  roles:
    - role: jmx_exporter
```


License
-------

BSD

Author Information
------------------

Jayendren Maduray <jayendren@gmail.com>
