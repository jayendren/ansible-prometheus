Ansible Prometheus
=========

### About

- prometheus:         http://prometheus.io

Requirements
------------

- FreeBSD https://www.freebsd.org/
- https://gitlab.com/jayendren/ansible-prometheus

Role Variables
--------------

```

    - role: prometheus
      prometheus_datadir: "/var/db/prometheus"
      global_scrape_interval: "15s"
      global_evaluation_interval: "15s"
      global_external_labels: "monitor"
      prom_cadvisor_exporter_nodes: 
        - coreos01.systemerror.co.za
        - coreos02.systemerror.co.za
        - docker.systemerror.co.za
      prom_cadvisor_port: 8080
      prom_jmx_tomcat_exporter_nodes:
        - app01.systemerror.co.za
      prom_jmx_tomcat_exporter_port: 7070        
      prom_exporters_multi:
        prometheus: |
          scrape_interval: 5s
              target_groups:
              - targets: ['localhost:9090']
        collectd_exporter: |
          scrape_interval: "15s"
              target_groups:
              - targets: [ 'localhost:9103' ]
            
```      

Dependencies
------------

- None

Example Playbook
----------------

Including an example of how to use the role (for instance, with variables passed in as parameters) is always nice for users too:

```
    - hosts: prometheus_cluster_nodes
      tasks: []
      roles:
        - role: golang

        - role: prometheus
          prometheus_datadir: "/var/db/prometheus"
          global_scrape_interval: "15s"
          global_evaluation_interval: "15s"
          global_external_labels: "monitor"
          prom_cadvisor_exporter_nodes: 
            - coreos01.systemerror.co.za
            - coreos02.systemerror.co.za
            - docker.systemerror.co.za
          prom_cadvisor_port: 8080
          prom_jmx_tomcat_exporter_nodes:
            - app01.systemerror.co.za
          prom_jmx_tomcat_exporter_port: 7070        
          prom_exporters_multi:
            prometheus: |
              scrape_interval: 5s
                  target_groups:
                  - targets: ['localhost:9090']
            collectd_exporter: |
              scrape_interval: "15s"
                  target_groups:
                  - targets: [ 'localhost:9103' ]
                 
```

License
-------

BSD

Author Information
------------------

Jayendren Maduray <jayendren@gmail.com>
